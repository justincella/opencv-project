# Simple example of the OpenCV library using video
# Justin Cella
# 11-20-19

import cv2 as cv
import time

# Initialize video capture
video = cv.VideoCapture(0)

a = 1

while True:
    a = a+1

    # create video frame by reading video caputre data
    check, frame = video.read()

    # print the frame matrix
    print(frame)

    # convert video frames to grey
    grey = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

    # title of window
    cv.imshow('Capturing', grey)

    # wait for a specific key
    key = cv.waitKey(1)

    # if key pressed is q, break while loop
    if key == ord('q'):
        break

print(a)

# release the video after loop
video.release()

# destroy all windows shown
cv.destroyAllWindows
