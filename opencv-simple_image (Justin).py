# Simple example of the OpenCV library using images to make a copy
# Justin Cella
# 11-20-19

import cv2 as cv

# Ask user for the image they want to call from the 'data' folder
image = str(input("Please enter the image name: "))

# Read image either -1(With alpha), 0(greyscale), 1(color) 
img = cv.imread('data/' + image, 0)

# Print the matrix of the image
print(img)

# Show window in new window ("name of window", image you want to show)
cv.imshow('image', img)

# Wait how long to show image for
cv.waitKey(5000)

# Destroy all windows until after wait
cv.destroyAllWindows()

# Create a copy of an image
cv.imwrite('copy_' + image, img)
