import numpy as np
import cv2
windowName = 'PythonProject'
img = np.zeros((512,512,3), np.uint8) #creates a plain black image
cv2.namedWindow(windowName)

def smiley(event, x, y, flags, param):
    if event == cv2.EVENT_RBUTTONDBLCLK:
        cv2.putText(img,'ILovePython',(205,390),cv2.FONT_HERSHEY_SIMPLEX,0.5,(255,0,0),1, cv2.LINE_AA) 
        cv2.putText(img,'ILovePython',(170,430),cv2.FONT_HERSHEY_SIMPLEX,1,(255,0,0),3, cv2.LINE_AA)
        cv2.putText(img,'ILovePython',(120,480),cv2.FONT_HERSHEY_SIMPLEX,1.5,(255,0,0),5, cv2.LINE_AA)
    if event == cv2.EVENT_RBUTTONDOWN:
        cv2.circle(img, (250,380),50, (255,255,0),-1)
    if event == cv2.EVENT_LBUTTONUP:
        cv2.ellipse(img,(160,180),(40,70),0,0,360,(0,0,255),5)
        cv2.ellipse(img,(340,180),(40,70),0,0,360,(0,0,255),5)
        cv2.circle(img, (250,270),230, (255,255,0),1)
        cv2.line(img,(220,380),(280,380),(0, 0, 255), 1)
        cv2.rectangle(img, (0,0),(510,510), (0, 0, 255), 3)
    if event == cv2.EVENT_LBUTTONDBLCLK:
        cv2.line(img,(150,300),(160,260),(255,255,255), 2)
        cv2.line(img,(350,300),(340,260),(255,255,255), 2)
        
cv2.setMouseCallback(windowName,smiley)
while (True):
        cv2.imshow(windowName, img)
        if cv2.waitKey(20) == 27:
            break
cv2.destroyAllWindows()
        
